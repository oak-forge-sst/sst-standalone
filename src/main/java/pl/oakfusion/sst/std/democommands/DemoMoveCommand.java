package pl.oakfusion.sst.std.democommands;

import lombok.Getter;
import pl.oakfusion.client.data.UserInput;
import pl.oakfusion.data.message.Command;

import static java.lang.Float.parseFloat;

public class DemoMoveCommand extends Command {

    @Getter
    boolean isAutomatic;
    float x;
    float y;

    public DemoMoveCommand(UserInput userInput) {
        isAutomatic = userInput.getCommandOptions().get(0).equals("AUTOMATIC");
        if (userInput.getCommandOptions().size() <= 3) {
            x = parseFloat(userInput.getCommandOptions().get(1));
            y = parseFloat(userInput.getCommandOptions().get(2));
        }
        switch (userInput.getCommandOptions().size()) {
            case 2:
                x = parseFloat(userInput.getCommandOptions().get(1));
                y = parseFloat(userInput.getCommandOptions().get(1));
                break;
            case 3:
                x = parseFloat(userInput.getCommandOptions().get(1));
                y = parseFloat(userInput.getCommandOptions().get(2));
                break;
            case 5:
                x = parseFloat(userInput.getCommandOptions().get(1)) + parseFloat(userInput.getCommandOptions().get(2));
                y = parseFloat(userInput.getCommandOptions().get(3)) + parseFloat(userInput.getCommandOptions().get(4));
        }
    }

    @Override
    public String toString() {
        return "DemoMoveCommand{" +
                "isAutomatic=" + isAutomatic +
                ", x=" + x +
                ", y=" + y +
                ", timestamp=" + timestamp +
                '}';
    }
}
