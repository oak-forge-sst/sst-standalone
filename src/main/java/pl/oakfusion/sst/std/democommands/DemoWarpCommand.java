package pl.oakfusion.sst.std.democommands;

import pl.oakfusion.client.data.UserInput;
import pl.oakfusion.data.message.Command;

import static java.lang.Float.parseFloat;

public class DemoWarpCommand extends Command {

    float warpFactor;

    public DemoWarpCommand(UserInput userInput) {
        warpFactor = parseFloat(userInput.getCommandOptions().get(0));
    }

    @Override
    public String toString() {
        return "DemoWarpCommand{" +
                "warpFactor=" + warpFactor +
                ", timestamp=" + timestamp +
                '}';
    }
}
