package pl.oakfusion.sst.std.core.viewrenderers;

import pl.oakfusion.client.Writer;
import pl.oakfusion.client.stream.StreamWriter;
import pl.oakfusion.client.view.ViewRenderer;
import pl.oakfusion.sst.std.core.events.hints.HintEvent;

public class HintViewRenderer implements ViewRenderer<HintEvent> {

    private final Writer writer;

    public HintViewRenderer() {
        this.writer = new StreamWriter(System.out);
    }

    @Override
    public void render(HintEvent event) {
        writer.writeLine(event.getPayload());
    }

}
