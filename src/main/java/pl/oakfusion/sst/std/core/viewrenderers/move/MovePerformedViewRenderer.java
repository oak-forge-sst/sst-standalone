package pl.oakfusion.sst.std.core.viewrenderers.move;

import pl.oakfusion.client.Writer;
import pl.oakfusion.sst.std.core.events.move.MovePerformedEvent;
import pl.oakfusion.client.stream.StreamWriter;
import pl.oakfusion.client.view.ViewRenderer;

public class MovePerformedViewRenderer implements ViewRenderer<MovePerformedEvent> {

    private Writer writer;

    public MovePerformedViewRenderer() {
        this.writer = new StreamWriter(System.out);
    }

    @Override
    public void render(MovePerformedEvent event) {
        writer.writeLine("You successfully moved to quadrant: " + event.getXQuadrant() + " - " + event.getYQuadrant() + ", sector: " + event.getXSector() + " - " + event.getYSector());
    }
}
