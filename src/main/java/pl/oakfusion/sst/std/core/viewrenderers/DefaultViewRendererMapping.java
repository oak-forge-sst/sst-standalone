package pl.oakfusion.sst.std.core.viewrenderers;

import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.client.view.ViewRendererMapping;
import pl.oakfusion.sst.std.core.events.hints.HintEvent;
import pl.oakfusion.sst.std.core.events.move.MovePerformedEvent;
import pl.oakfusion.sst.std.core.viewrenderers.move.MovePerformedViewRenderer;

public class DefaultViewRendererMapping implements ViewRendererMapping {

    private MessageBus messageBus;

    public DefaultViewRendererMapping(MessageBus messageBus) {
        this.messageBus = messageBus;
    }

    public void registerViewHandlers() {
        messageBus.registerMessageHandler(HintEvent.class, new HintViewRenderer()::render);
        messageBus.registerMessageHandler(MovePerformedEvent.class, new MovePerformedViewRenderer()::render);
    }

}
