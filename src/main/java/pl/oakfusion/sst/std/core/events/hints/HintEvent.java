package pl.oakfusion.sst.std.core.events.hints;

import lombok.*;
import pl.oakfusion.data.message.Event;

@Getter
@Setter
public class HintEvent extends Event {
    private final String payload;

    public HintEvent(String payload) {
        this.payload = payload;
    }
}
