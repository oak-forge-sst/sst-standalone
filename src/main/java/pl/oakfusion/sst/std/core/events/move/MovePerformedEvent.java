package pl.oakfusion.sst.std.core.events.move;

import lombok.Getter;
import pl.oakfusion.data.message.Event;

import java.time.LocalDateTime;

@Getter
public class MovePerformedEvent extends Event {

    private int xQuadrant;
    private int yQuadrant;
    private int xSector;
    private int ySector;

    public MovePerformedEvent(LocalDateTime timestamp, int xQuadrant, int yQuadrant, int xSector, int ySector) {
        super(timestamp);
        this.xQuadrant = xQuadrant;
        this.yQuadrant = yQuadrant;
        this.xSector = xSector;
        this.ySector = ySector;
    }
}
