package pl.oakfusion.sst.std;

public class DemoGuavaBus {

	/*
	CommandBuilderFactory pl.oakfusion.sst.data.newcommands.factory;
	FeatureSwitchHandler featureSwitchHandler;
	GuavaEventBus guavaEventBus;
	EventBus eventBus = new EventBus();


	public DemoGuavaBus(CommandBuilderFactory pl.oakfusion.sst.data.newcommands.factory, FeatureSwitchHandler featureSwitchHandler) {
		this.pl.oakfusion.sst.data.newcommands.factory = pl.oakfusion.sst.data.newcommands.factory;
		this.featureSwitchHandler = featureSwitchHandler;

		guavaEventBus = new GuavaEventBus(eventBus);

		guavaBusStart(pl.oakfusion.sst.data.newcommands.factory, guavaEventBus, featureSwitchHandler);
	}

	public static void guavaBusStart(CommandBuilderFactory pl.oakfusion.sst.data.newcommands.factory, GuavaEventBus guavaEventBus, FeatureSwitchHandler featureSwitchHandler) {

		ConsoleClient client = new ConsoleClient(pl.oakfusion.sst.data.newcommands.factory, guavaEventBus);

		Application application = new Application(new RandomGenerator(), guavaEventBus);

		guavaEventBus.registerMessageHandler(Command.class, application);

		featureSwitchHandler.auditLogToggle(guavaEventBus);

		guavaEventBus.registerMessageHandler(InitializeConfiguredGame.class, cmd -> System.out.println(cmd.getSpecification()));

		guavaEventBus.post(new InitializeApplication());
	}
	/**/
}

