package pl.oakfusion.sst.std;

import lombok.NoArgsConstructor;
import pl.oakfusion.client.data.UserInput;
import pl.oakfusion.client.factory.MessageFactory;
import pl.oakfusion.data.message.Command;
import pl.oakfusion.sst.std.democommands.*;

@NoArgsConstructor
class DemoMessageFactory implements MessageFactory<Command> {

    @Override
    public Command createMessage(UserInput userInput) {
        if (userInput.getMnemonic().equals("WARP")) {
            return new DemoWarpCommand(userInput);
        } else {
            return new DemoMoveCommand(userInput);
        }
    }

}
