package pl.oakfusion.sst.std;

import com.google.common.eventbus.EventBus;
import lombok.extern.slf4j.Slf4j;
import pl.oakfusion.bus.guava.GuavaMessageBus;
import pl.oakfusion.client.Client;
import pl.oakfusion.client.parser.UserInputParser;
import pl.oakfusion.sst.std.core.viewrenderers.DefaultViewRendererMapping;
import pl.oakfusion.sst.std.democommands.*;

import java.io.IOException;

@Slf4j
public class Demo {

    @SuppressWarnings("UnstableApiUsage")
    private final EventBus eventBus = new EventBus();
    private final GuavaMessageBus guavaMessageBus = new GuavaMessageBus(eventBus);
    private final DemoMessageFactory demoMessageFactory = new DemoMessageFactory();
    private final DefaultViewRendererMapping defaultViewRendererMapping = new DefaultViewRendererMapping(guavaMessageBus);
    private final UserInputParser userInputParser = new UserInputParser();
    private final Client client = new TerminalClient(demoMessageFactory, guavaMessageBus, defaultViewRendererMapping, userInputParser);

    public Demo() throws IOException {
    }

    public static void main(String[] args) throws IOException {
        new Demo().run();
    }

    private void run() {
        log.info("Starting");
        guavaMessageBus.registerMessageHandler(DemoWarpCommand.class, this::handleDemoWarpCommand);
        guavaMessageBus.registerMessageHandler(DemoMoveCommand.class, this::handleDemoMoveCommand);
        while (true) {
            client.handleMessageFromUser();
        }
    }

    private void handleDemoMoveCommand(DemoMoveCommand demoMoveCommand) {
        System.out.println(demoMoveCommand.toString());
    }

    private void handleDemoWarpCommand(DemoWarpCommand demoWarpCommand) {
        System.out.println(demoWarpCommand.toString());
    }
}
