package pl.oakfusion.sst.std;

public class DemoRabbitBus {

	/*
	CommandBuilderFactory pl.oakfusion.sst.data.newcommands.factory;
	PlayersRepository playersRepository;
	FeatureSwitchHandler featureSwitchHandler;
	RabbitMQ rabbitMQ = new RabbitMQ();
	RabbitBus rabbitBus;

	public DemoRabbitBus(CommandBuilderFactory pl.oakfusion.sst.data.newcommands.factory,
						 FeatureSwitchHandler featureSwitchHandler) {
		this.pl.oakfusion.sst.data.newcommands.factory = pl.oakfusion.sst.data.newcommands.factory;
		this.rabbitBus = new RabbitBus(rabbitMQ);
		this.featureSwitchHandler = featureSwitchHandler;
		rabbitMQ.startRabbitMQ();
		rabbitBusStart(pl.oakfusion.sst.data.newcommands.factory, rabbitBus, playersRepository, featureSwitchHandler);
	}

	public static void rabbitBusStart(CommandBuilderFactory pl.oakfusion.sst.data.newcommands.factory, RabbitBus rabbitBus,
									  PlayersRepository playersRepository, FeatureSwitchHandler featureSwitchHandler) {

	//	ConsoleClient client = new ConsoleClient(pl.oakfusion.sst.data.newcommands.factory, rabbitBus);

		Application application = new Application(new RandomGenerator(), rabbitBus);


		rabbitBus.registerMessageHandler(Command.class, application);

		featureSwitchHandler.auditLogToggle(rabbitBus);

		rabbitBus.registerMessageHandler(InitializeConfiguredGame.class, cmd -> System.out.println(cmd.getSpecification()));

		rabbitBus.post(new InitializeApplication());
	}
	/**/
}
