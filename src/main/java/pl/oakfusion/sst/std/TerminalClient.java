package pl.oakfusion.sst.std;

import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.client.Client;
import pl.oakfusion.client.data.*;
import pl.oakfusion.client.factory.MessageFactory;
import pl.oakfusion.client.parser.UserInputParser;
import pl.oakfusion.client.stream.*;
import pl.oakfusion.client.view.ViewRendererMapping;
import pl.oakfusion.data.message.Command;
import pl.oakfusion.sst.std.core.events.hints.HintEvent;

import java.io.IOException;

class TerminalClient extends Client {

    protected TerminalClient(MessageFactory<Command> messageFactory, MessageBus messageBus, ViewRendererMapping viewRendererMapping, UserInputParser userInputParser) {
        super(new StreamReader(System.in), new StreamWriter(System.out), messageFactory, messageBus, viewRendererMapping , userInputParser);
    }


    protected UserInput readUserInput() {
        try {
            UserInputParsingResult userInputParsingResult = userInputParser.parseInput(reader.read());
            while (userInputParsingResult.getHints()!=null) {
                post(new HintEvent(userInputParsingResult.getHints()));
                userInputParsingResult = userInputParser.parseInput(reader.read());
            }
            return userInputParsingResult.getUserInput();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new UserInput();
    }
}
